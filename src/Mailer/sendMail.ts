import * as nodemailer from 'nodemailer';
import { Injectable } from '@nestjs/common';
@Injectable()
export class MailService {
    private _transporter: nodemailer.Transporter;
    constructor() {
        this._transporter = nodemailer.createTransport(
            {
                service: 'gmail',
                auth: {
                    user: 'anh.cao@sotatek.com',
                    pass: 'Anhanh11@'
                }
            }
        );
    }
    sendMail(subject: string, text: string, email: string): void {
        const options = {
            from: 'anh.cao@sotatek.com',
            to: email,
            subject: subject,
            text: text
        };
        this._transporter.sendMail(
            options, (error, info) => {
                if (error) {
                    return console.log(`error: ${error}`);
                }
                console.log(`Message Sent ${info.response}`);
            });
    }
} 
