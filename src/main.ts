import { NestFactory } from '@nestjs/core';
import { Transport } from '@nestjs/microservices';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.createMicroservice(AppModule, {
    transport: Transport.RMQ,
    options: {
      urls: [
        'amqps://aombmvvl:sDAN1RFS5JU6nFaymymaMJ0VR3h3vb6j@snake.rmq2.cloudamqp.com/aombmvvl',
      ],
      queue: 'main_queue',
      queueOptions: {
        durable: false,
      },
    },
  });
  app.listen(() => console.log('Microservice is listening'));
}
bootstrap();
