import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { MailService } from "../Mailer/sendMail"
import * as crypto from 'crypto';
import { accessKey, apiEndpoint, partnerCode, secretKey } from './momo-info';
import { v1 as uuidv1 } from 'uuid';
import axios from 'axios';

@Injectable()
export class PaymentService {
  constructor(
   private readonly mailService:MailService
  ) {}

  /**
   *
   * @param data data send momo
   * @param secretkey register account momo company
   * @returns string
   */

  async hashSignature(data: any, secretkey: string): Promise<string> {
    try {
      const signature = crypto
        .createHmac('sha256', secretkey)
        .update(data)
        .digest('hex');
      return signature;
    } catch (error) {
      console.log(error);
    }
  }

  async paymentByWallet(amount: string, email: string) {
   try {
    const genId = uuidv1();
    const rawData = `partnerCode=${partnerCode}&accessKey=${accessKey}&requestId=${genId}&amount=${String(
      amount,
    )}&orderId=${genId}&orderInfo=Anh Cao Sotatek&returnUrl=https://momo.vn&notifyUrl=https://momo.vn&extraData=${email}`;

    const signature = await this.hashSignature(rawData, secretKey);
    const dataSendApiMomo = {
      accessKey,
      partnerCode,
      requestType: 'captureMoMoWallet',
      notifyUrl: 'https://momo.vn',
      returnUrl: 'https://momo.vn',
      orderId: genId,
      amount: String(amount),
      orderInfo: 'Anh Cao Sotatek',
      requestId: genId,
      extraData: email,
      signature,
    };
    const result = await axios({
      url: apiEndpoint,
      method: 'POST',
      data: dataSendApiMomo,
    });
    await this.sendEmailToClient(email)
    console.log(result.data);
    return {
        status:200,
        messages:"Success!",
        data:result.data
    }
   } catch (error) {
       throw new HttpException("Bad request",HttpStatus.BAD_REQUEST)
   }
  }

  async paymentCod (email:string) {
    try {
        await this.sendEmailToClient(email)
        return {
            status:200,
            messages:"Success"
        }
    } catch (error) {
        
    }
  }

  async sendEmailToClient(email: string): Promise<void> {
    const html = `
        Your order has been successfully created!
        Thank you!
        ────────────────
        Sotatek
        ────────────────
        `;
    await this.mailService.sendMail('Payment', html, email);
}
}
