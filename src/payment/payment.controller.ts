import { Body, Controller, Inject, Post } from '@nestjs/common';
import { EventPattern } from '@nestjs/microservices';
import { PaymentService } from './payment.service';

@Controller('payment')
export class PaymentController {
    constructor(
        private readonly paymentService: PaymentService,
    ) { }
    @EventPattern('wallet')
   async paymentByWallet(data: any) {
        const { amount, email } = data;
        await this.paymentService.paymentByWallet(amount, email)
    }

    @Post("cod")
    paymentByCod(@Body() data:any) {
        const { email } = data;
       
        return this.paymentByCod(email)
    }
}
