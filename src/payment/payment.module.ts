import { Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { MailService } from 'src/Mailer/sendMail';
import { PaymentController } from './payment.controller';
import { PaymentService } from './payment.service';

@Module({
  imports:[
    ClientsModule.register([
      {
        name: 'PAYMENT_SERVICE',
        transport: Transport.RMQ,
        options: {
          urls: ['amqps://aombmvvl:sDAN1RFS5JU6nFaymymaMJ0VR3h3vb6j@snake.rmq2.cloudamqp.com/aombmvvl'],
          queue: 'main_queue',
          queueOptions: {
            durable: false
          },
        },
      },
    ]),
  ],
  controllers: [PaymentController],
  providers: [PaymentService,MailService]
})
export class PaymentModule {}
